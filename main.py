from mysql.connector import connect, Error
from mysql.connector.abstracts import MySQLConnectionAbstract
from mysql.connector.pooling import PooledMySQLConnection
import time


def main():
    try:
        with connect(host="localhost", user="root", password="", database="uir-lab5", port=3305) as connection:
            professions = get_professions(connection)

            with connection.cursor(buffered=True) as cursor:
                start = time.time()
                for i in range(200):
                    for profession in professions:
                        cursor.execute("SELECT * FROM people_with_partitioning WHERE profession = '" + profession + "'")
                end = time.time()
                print("Time taken WITH partitioning: " + str(end - start))

                start = time.time()
                for i in range(200):
                    for profession in professions:
                        cursor.execute("SELECT * FROM people_no_partitioning WHERE profession = '" + profession + "'")
                end = time.time()
                print("Time taken WITHOUT partitioning: " + str(end - start))

    except Error as e:
        print(e)


def get_professions(connection: PooledMySQLConnection | MySQLConnectionAbstract) -> list[str]:
    get_profs_query = "SELECT DISTINCT profession FROM people_no_partitioning"
    professions = []

    with connection.cursor() as cursor:
        cursor.execute(get_profs_query)
        for profession in cursor:
            professions.append(profession[0])

    return professions


if __name__ == '__main__':
    main()
